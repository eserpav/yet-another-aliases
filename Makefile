all:
	@echo Bothing changed, all done!

diff:
	diff -s .alias ~/.alias

install:
	ansible-playbook -v setup.yaml

install_all:
	ansible-playbook -v -i ~/ansible/hosts setup.yaml
