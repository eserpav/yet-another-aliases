# Yet another aliases

Just my BASH aliases

## Installation

You can use Ansible to install it, or do the same manually

```bash
ansible-playbook -v -i ~/ansible/hosts setup.yaml
```

setup.yaml
```yaml
- hosts: localhost:all
  become: no
  gather_facts: yes
  tasks:

    - name: Source .alias in .bashrc
      lineinfile:
        dest: "{{ ansible_env.HOME }}/.bashrc"
        state: present
        regexp: 'source \$HOME/.alias\s*$'
        line: '[ -f $HOME/.alias ] && source $HOME/.alias'

    - name: Check .zshrc exists
      stat:
        path: "{{ ansible_env.HOME }}/.zshrc"
      register: zshrc

    - name: Source .alias in .zshrc
      lineinfile:
        dest: "{{ ansible_env.HOME }}/.zshrc"
        state: present
        regexp: 'source \$HOME/.alias\s*$'
        line: '[ -f $HOME/.alias ] && source $HOME/.alias'
      when: zshrc.stat.exists

    - name: Download latest .alias
      get_url:
        url: https://gitlab.com/eserpav/yet-another-aliases/-/raw/main/.alias?inline=false
        dest: "{{ ansible_env.HOME }}/.alias"
        force: yes

```
## Usage
See content of the [.alias](https://gitlab.com/eserpav/yet-another-aliases/-/blob/main/.alias) file for available commands

## License
[GNU General Public License v3](http://www.gnu.org/licenses/gpl-3.0.html) or later

## Project status
Should be updated from time to time
